// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
  	apiKey: "AIzaSyAIC0OLu5Zs7q2Br7ruITUWJ0oJYedTS5A",
    authDomain: "fuego-7449d.firebaseapp.com",
    databaseURL: "https://fuego-7449d.firebaseio.com",
    projectId: "fuego-7449d",
    storageBucket: "fuego-7449d.appspot.com",
    messagingSenderId: "817456137442",
    appId: "1:817456137442:web:417d9af9703e3187"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
