import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from '../shared/login/login.component';
import { IonicModule } from '@ionic/angular';
import { ProfileComponent } from './profile/profile.component';


@NgModule({
  imports: [CommonModule, IonicModule],
  declarations: [LoginComponent, ProfileComponent],
  exports: [LoginComponent, ProfileComponent]
})
export class SharedModule { }
